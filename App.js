import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Splash from './Splash'
import Main from './Main'
import BarcodeCamera from './BarcodeCamera'
import Counter from './Counter'

const MainNavigator = createStackNavigator({
  Splash: {
    screen: Splash,
  },
  Main: {
    screen: Main,
  },
  BarcodeCamera: {
    screen: BarcodeCamera,
  },
  Counter: {
    screen: Counter,
  },
});
const Navigation = createAppContainer(MainNavigator);
export default Navigation;