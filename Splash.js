import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  Alert,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
  Dimensions,
  Modal,
  View,
  Image,
  PixelRatio
} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

var width = Dimensions.get('window').width * PixelRatio.get();
var height = Dimensions.get('window').height * PixelRatio.get();
export default class Splash extends Component {
  static navigationOptions = {
    header: null
  }
      componentDidMount()
  {
      setTimeout(()=>{
          this.props.navigation.navigate('Main');
      },2000)
      }



  render() {
    // Image.getSize(source, (width, height) => {this.setState({width, height})});
    // alert(height + "--" + width)
    if(Platform.OS=='android'){
    if (width >= 240 && width < 320) {
      return (


        <ImageBackground style={styles.image}
          source={require('./images/240X320/splashbackground.png')} >
          <View style={{ flex: 1,justifyContent: 'center',alignItems: 'center' }}>
          <Image style={{ width:124, height: 44 ,resizeMode: 'center' }} source={require('./images/240X320/logo.png')} >
          </Image>
          </View>
        </ImageBackground>
      );
    } else if (width >= 320 && width < 480) {
      return (
        <ImageBackground style={styles.image}
          source={require('./images/320X480/splashbackground.png')} >
          <View style={{ flex: 1,justifyContent: 'center',alignItems: 'center' }}>
          <Image style={{ width:165, height:65,resizeMode: 'center' }} source={require('./images/320X480/logo.png')} >
          </Image>
          </View>
        </ImageBackground>
      );
    }
    else if (width >= 480 && width < 540) {
      return (
        <ImageBackground style={styles.image}
          source={require('./images/480X800/splashbackground.png')} >
          <View style={{ top: height / 5, alignItems: 'center' }}>
            <Image style={{ width:247, height:108 ,resizeMode: 'center' }} source={require('./images/480X800/logo.png')} >
            </Image>
          </View>
        </ImageBackground>
      );
    }
    else if (width >= 540 && width < 720) {
      return (
        <ImageBackground style={styles.image}
          source={require('./images/540X960/splashbackground.png')} >
          <View style={{ flex: 1,justifyContent: 'center',alignItems: 'center' }}>
          <Image style={{ width:278, height:130,resizeMode: 'center' }} source={require('./images/540X960/logo.png')} >
          </Image>
          </View>
        </ImageBackground>
      );
    }
    else if (width >= 720 && width < 800) {
      return (
        <ImageBackground style={styles.image}
          source={require('./images/720X1280/splashbackground.png')} >
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Image style={{ width: 372, height: 173,resizeMode: 'center' }}
              source={require('./images/720X1280/logo.png')} >
            </Image>
          </View>
        </ImageBackground>
      );
    }
    else if (width >= 800 && width < 1080) {
      return (
        <ImageBackground style={styles.image}
          source={require('./images/800X1280/splashbackground.png')} >
          <View style={{ flex: 1, justifyContent: 'center',  alignItems: 'center'}}>
            <Image style={{ width: 412, height: 173,resizeMode: 'center' }} source={require('./images/800X1280/logo.png')} >
            </Image>
          </View>
        </ImageBackground>
      );
    }
    else if (width >= 1080 && width < 1440) {
      return (
        <ImageBackground style={styles.image}
          source={require('./images/1080X1920/splashbackground.png')} >
          <View style={{ flex: 1, justifyContent: 'center',  alignItems: 'center'}}>
            <Image style={{ width: 555, height: 259,resizeMode: 'center' }} source={require('./images/1080X1920/logo.png')} >
            </Image>
          </View>
        </ImageBackground>
      );
    }
    else if (width >= 1440) {
      return (
        <ImageBackground style={styles.image}
          source={require('./images/1440X2560/splashbackground.png')} >
           <View style={{ flex: 1, justifyContent: 'center',  alignItems: 'center'}}>
            <Image style={{ width: 739, height: 344, resizeMode: 'center' }} source={require('./images/1440X2560/logo.png')} >
            </Image>
          </View>
        </ImageBackground>
      );
    }
  }else{
    if (width >= 640 && width < 750) {
      return (


        <ImageBackground style={styles.image}
          source={require('./images/iPhone5/Layer-27.png')} >
          <View style={{ flex: 1,justifyContent: 'center',alignItems: 'center' }}>
          <Image style={{ width:329, height: 153 }} source={require('./images/iPhone5/Layer-19.png')} >
          </Image>
          </View>
        </ImageBackground>
      );
    } else if (width >= 750 && width < 1080) {
      return (
        <ImageBackground style={styles.image}
          source={require('./images/iphone6/Layer-28.png')} >
          <View style={{ flex: 1,justifyContent: 'center',alignItems: 'center' }}>
          <Image style={{ width:329, height:153 }} source={require('./images/iphone6/background.png')} >
          </Image>
          </View>
        </ImageBackground>
      );
    }
    else if (width >= 1080 && width < 1440) {
      return (
        <ImageBackground style={styles.image}
          source={require('./images/1080X1920/splashbackground.png')} >
          <View style={{ flex: 1, justifyContent: 'center',  alignItems: 'center'}}>
            <Image style={{ width: 555, height: 259,resizeMode: 'center' }} source={require('./images/1080X1920/logo.png')} >
            </Image>
          </View>
        </ImageBackground>
      );
    }
  }
  }
}


const styles = StyleSheet.create({
  container: {
    padding: 30,
    // marginTop: 65,
    alignItems: 'center'
    //background-image:url("android/app/src/main/res/mipmap-hdpi/background.jpg" )
  },
  image: {
    flex: 1,
    width: null,
    height: null,

    // resizeMode: 'contain'
  },
  logo: {
    width: 412,
    height: 173,



  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  }

});
