import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    Alert,
    TextInput,
    TouchableOpacity,
    ImageBackground,
    ActivityIndicator,
    Dimensions,
    Modal,
    View,
    Image,
    PixelRatio
} from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' +
        'Cmd+D or shake for dev menu',
    android: 'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

var width = Dimensions.get('window').width * PixelRatio.get();
var height = Dimensions.get('window').height * PixelRatio.get();
export default class Main extends Component {
    static navigationOptions = {
        header: null
    }
    anotherFunction = () => {
        // this.props.navigation.navigate('BarcodeCamera');
        this.props.navigation.navigate('Counter', {
            item: {
                "barcodeSuffix": 1,
                "itemType": "drug",
                "subUnit": "tab",
                "activeMaterial": "amlodipine 5mg",
                "searchKeywords": "",
                "usage": "antihypertensive",
                "origin": "Egypt",
                "salesPrice": 30,
                "expirable": true,
                "frequent": false,
                "accDiscount": 25,
                "storage": "None",
                "narcotic": false,
                "subUnitsPerPkg": 30,
                "pricePerUnit": 10,
                "salesCategory": "None",
                "primaryCategory": "None",
                "barcodePrefix": "0",
                "drugID": 29,
                "unitsPerPkg": "None",
                "comments": "",
                "minStock": 1,
                "pricePerSubUnit": 1,
                "secondaryCategory": "None",
                "unit": "strips",
                "pkgDescription": "30tab",
                "form": "tab",
                "intBarcode": "6224000875305",
                "name": "amilo 5",
                "producer": "alfacure",
                "maxSalesDiscount": 10
              },
              expiryDate: " تاريخ الصلاحية : 10/12/2019"
          })
    }
    render() {
        // Image.getSize(source, (width, height) => {this.setState({width, height})});
        if (Platform.OS == 'android') {
            if (width >= 240 && width < 320) {
                return (
                    <ImageBackground style={styles.image}
                        source={require('./images/240X320/splashbackground.png')} >
                        <View style={{ alignItems: 'center', alignContent: 'center', flexDirection: 'column' }}>
                            <Image style={{ width: 124 / PixelRatio.get(), height: 44 / PixelRatio.get(), resizeMode: 'center', margin: 10 }}
                                source={require('./images/240X320/logo.png')} >
                            </Image>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity style={[styles.loginButton, { width: 187 / PixelRatio.get(), height: 29 / PixelRatio.get(), }]}>
                                <Text numberOfLines={1} style={styles.loginButtonText}>Scan Item Barcode</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={[styles.guestButton, { width: 187 / PixelRatio.get(), height: 29 / PixelRatio.get(), }]}>
                                <Text numberOfLines={1} style={styles.guestButtonText}>Scan Item Name</Text>
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                );
            } else if (width >= 320 && width < 480) {
                return (
                    <ImageBackground style={styles.image}
                        source={require('./images/320X480/splashbackground.png')} >
                        <View style={{ alignItems: 'center', alignContent: 'center', flexDirection: 'column' }}>
                            <Image style={{ width: 165 / PixelRatio.get(), height: 65 / PixelRatio.get(), resizeMode: 'center', margin: 10 }}
                                source={require('./images/320X480/logo.png')} >
                            </Image>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity style={[styles.loginButton, { width: 249 / PixelRatio.get(), height: 44 / PixelRatio.get(), }]}>
                                <Text numberOfLines={1} style={styles.loginButtonText}>Scan Item Barcode</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={[styles.guestButton, { width: 249 / PixelRatio.get(), height: 44 / PixelRatio.get(), }]}>
                                <Text numberOfLines={1} style={styles.guestButtonText}>Scan Item Name</Text>
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                );
            }
            else if (width >= 480 && width < 540) {
                return (
                    <ImageBackground style={styles.image}
                        source={require('./images/480X800/splashbackground.png')} >
                        <View style={{ alignItems: 'center', alignContent: 'center', flexDirection: 'column' }}>
                            <Image style={{ width: 247 / PixelRatio.get(), height: 108 / PixelRatio.get(), resizeMode: 'center', margin: 10 }}
                                source={require('./images/480X800/logo.png')} >
                            </Image>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity style={[styles.loginButton, { width: 372 / PixelRatio.get(), height: 73 / PixelRatio.get(), }]}>
                                <Text numberOfLines={1} style={styles.loginButtonText}>Scan Item Barcode</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={[styles.guestButton, { width: 372 / PixelRatio.get(), height: 73 / PixelRatio.get(), }]}>
                                <Text numberOfLines={1} style={styles.guestButtonText}>Scan Item Name</Text>
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                );
            }
            else if (width >= 540 && width < 720) {
                return (
                    <ImageBackground style={styles.image}
                        source={require('./images/540X960/splashbackground.png')} >
                        <View style={{ alignItems: 'center', alignContent: 'center', flexDirection: 'column' }}>
                            <Image style={{ width: 278 / PixelRatio.get(), height: 130 / PixelRatio.get(), resizeMode: 'center', margin: 10 }}
                                source={require('./images/540X960/logo.png')} >
                            </Image>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity style={[styles.loginButton, { width: 419 / PixelRatio.get(), height: 87 / PixelRatio.get(), }]}>
                                <Text numberOfLines={1} style={styles.loginButtonText}>Scan Item Barcode</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={[styles.guestButton, { width: 419 / PixelRatio.get(), height: 87 / PixelRatio.get(), }]}>
                                <Text numberOfLines={1} style={styles.guestButtonText}>Scan Item Name</Text>
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                );
            }
            else if (width >= 720 && width < 800) {
                return (
                    <ImageBackground style={styles.image}
                        source={require('./images/720X1280/splashbackground.png')} >
                        <View style={{ alignItems: 'center', alignContent: 'center', flexDirection: 'column' }}>
                            <Image style={{ width: 372 / PixelRatio.get(), height: 173 / PixelRatio.get(), resizeMode: 'center', margin: 10 }}
                                source={require('./images/720X1280/logo.png')} >
                            </Image>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity onPress={this.anotherFunction.bind(this)} style={[styles.loginButton, { width: 559 / PixelRatio.get(), height: 116 / PixelRatio.get(), }]}>
                                <Text numberOfLines={1} style={styles.loginButtonText}>Scan Item Barcode</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={[styles.guestButton, { width: 559 / PixelRatio.get(), height: 116 / PixelRatio.get(), }]}>
                                <Text numberOfLines={1} style={styles.guestButtonText}>Scan Item Name</Text>
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                );
            }
            else if (width >= 800 && width < 1080) {
                return (
                    <ImageBackground style={styles.image}
                        source={require('./images/800X1280/splashbackground.png')} >
                        <View style={{ alignItems: 'center', alignContent: 'center', flexDirection: 'column' }}>
                            <Image style={{ width: 412 / PixelRatio.get(), height: 173 / PixelRatio.get(), resizeMode: 'center', margin: 10 }}
                                source={require('./images/800X1280/logo.png')} >
                            </Image>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity onPress={this.anotherFunction.bind(this)} style={[styles.loginButton, { width: 620 / PixelRatio.get(), height: 116 / PixelRatio.get(), }]}>
                                <Text numberOfLines={1} style={styles.loginButtonText}>Scan Item Barcode</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={[styles.guestButton, { width: 620 / PixelRatio.get(), height: 116 / PixelRatio.get(), }]}>
                                <Text numberOfLines={1} style={styles.guestButtonText}>Scan Item Name</Text>
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                );
            }
            else if (width >= 1080 && width < 1440) {
                return (
                    <ImageBackground style={styles.image}
                        source={require('./images/1080X1920/splashbackground.png')} >
                        <View style={{ alignItems: 'center', alignContent: 'center', flexDirection: 'column' }}>
                            <Image style={{ width: 555 / PixelRatio.get(), height: 259 / PixelRatio.get(), resizeMode: 'center', margin: 10 }}
                                source={require('./images/1080X1920/logo.png')} >
                            </Image>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity onPress={this.anotherFunction.bind(this)} style={[styles.loginButton, { width: 837 / PixelRatio.get(), height: 173 / PixelRatio.get(), }]}>
                                <Text numberOfLines={1} style={styles.loginButtonText}>Scan Item Barcode</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={[styles.guestButton, { width: 837 / PixelRatio.get(), height: 173 / PixelRatio.get(), }]}>
                                <Text numberOfLines={1} style={styles.guestButtonText}>Scan Item Name</Text>
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                );
            }
            else if (width >= 1440) {
                return (
                    <ImageBackground style={styles.image}
                        source={require('./images/1440X2560/splashbackground.png')} >
                        <View style={{ alignItems: 'center', alignContent: 'center', flexDirection: 'column' }}>
                            <Image style={{ width: 739 / PixelRatio.get(), height: 344 / PixelRatio.get(), resizeMode: 'center', margin: 10 }}
                                source={require('./images/1440X2560/logo.png')} >
                            </Image>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <TouchableOpacity onPress={this.anotherFunction.bind(this)} style={[styles.loginButton, { width: 1116 / PixelRatio.get(), height: 231 / PixelRatio.get(), }]}>
                                <Text numberOfLines={1} style={styles.loginButtonText}>Scan Item Barcode</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={[styles.guestButton, { width: 1116 / PixelRatio.get(), height: 231 / PixelRatio.get(), }]}>
                                <Text numberOfLines={1} style={styles.guestButtonText}>Scan Item Name</Text>
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                );
            }
        } else {
            if (width >= 640 && width < 750) {
                return (


                    <ImageBackground style={styles.image}
                        source={require('./images/iPhone5/Layer-27.png')} >
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={{ width: 329, height: 153 }} source={require('./images/iPhone5/Layer-19.png')} >
                            </Image>
                        </View>
                    </ImageBackground>
                );
            } else if (width >= 750 && width < 1080) {
                return (
                    <ImageBackground style={styles.image}
                        source={require('./images/iphone6/Layer-28.png')} >
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={{ width: 329, height: 153 }} source={require('./images/iphone6/background.png')} >
                            </Image>
                        </View>
                    </ImageBackground>
                );
            }
            else if (width >= 1080 && width < 1440) {
                return (
                    <ImageBackground style={styles.image}
                        source={require('./images/1080X1920/splashbackground.png')} >
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={{ width: 555, height: 259, resizeMode: 'center' }} source={require('./images/1080X1920/logo.png')} >
                            </Image>
                        </View>
                    </ImageBackground>
                );
            }
        }
    }
}


const styles = StyleSheet.create({
    container: {
        padding: 30,
        // marginTop: 65,
        alignItems: 'center'
        //background-image:url("android/app/src/main/res/mipmap-hdpi/background.jpg" )
    },
    image: {
        flex: 1,
        width: null,
        height: null,

        // resizeMode: 'contain'
    },
    logo: {
        width: 412,
        height: 173,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    loginButton: {
        borderRadius: 27,
        backgroundColor: '#1e2f4d',
        justifyContent: 'center',
        opacity: .9,

    },
    loginButtonText: {
        fontWeight: 'bold',
        fontSize: 16,
        color: 'white',
        textAlign: 'center',
    },
    guestButton: {
        borderRadius: 27,
        backgroundColor: '#939090',
        justifyContent: 'center',
        opacity: .9,
        margin: 20
    },
    guestButtonText: {
        fontWeight: 'bold',
        fontSize: 16,
        color: 'white',
        textAlign: 'center',
    },

});
