/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  View,
  Image,
  PixelRatio,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

var width = Dimensions.get('screen').width * PixelRatio.get();

export default class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      checked: false,
      packageText: "",
      unitsFlag: true,
    }
  }
  componentDidMount(){
    const { navigation } = this.props;
    var test = navigation.getParam('item', '')
    var expiry = navigation.getParam('expiryDate', '')
    if (test.unitsPerPkg == "None") {
      this.setState({
        unitsFlag : false
      })
    } else {
      this.setState({
        unitsFlag : true
      })
    }
  }
  static navigationOptions = {
    header: null
  }

  ShowHideActivityIndicator = () => {

    if (this.state.isLoading == true) {
      this.setState({ isLoading: false })
    }
    else {
      this.setState({ isLoading: true })
    }
  }


  anotherFunction = () => {
    this.props.navigation.navigate('home');
  }


  render() {

    const { navigation } = this.props;
    var test = navigation.getParam('item', '')
    var expiry = navigation.getParam('expiryDate', '')
    // if (test.unitsPerPkg == "None") {
    //   this.setState({
    //     unitsFlag : false
    //   })
    // } else {
    //   this.setState({
    //     unitsFlag : true
    //   })
    // }
    var name = test.name
    if (Platform.OS == 'android') {
      if (width >= 240 && width < 320) {

        return (
          <ImageBackground style={styles.image}
            source={require('./images/240X320/splashbackground.png')} >
            <KeyboardAwareScrollView>
              <View style={styles.container}>
                <View style={styles.SectionStyle}>
                  <Image source={require('./images/240X320/personicon.png')} style={styles.ImageStyle}>
                  </Image>
                  <TextInput
                    placeholder="اسم الصنف"
                    placeholderTextColor="black"
                    onChangeText={userName => this.setState({ userName })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 188 / PixelRatio.get(), height: 32 / PixelRatio.get(), }]} />

                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>
                  <Image source={require('./images/240X320/keyicon.png')} style={styles.ImageStyle}>
                  </Image>
                  <TextInput
                    placeholder="تاريخ الصلاحية"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 188 / PixelRatio.get(), height: 32 / PixelRatio.get(), }]} />
                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>
                  <Image source={require('./images/240X320/keyicon.png')} style={styles.ImageStyle}>
                  </Image>
                  <TextInput
                    placeholder="إدخل عدد الصنف"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 188 / PixelRatio.get(), height: 32 / PixelRatio.get(), }]} />
                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>
                  <Image source={require('./images/240X320/keyicon.png')} style={styles.ImageStyle}>
                  </Image>
                  <TextInput
                    placeholder="إدخل عدد الوحدات بالصنف"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 188 / PixelRatio.get(), height: 32 / PixelRatio.get(), }]} />
                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>
                  <Image source={require('./images/240X320/keyicon.png')} style={styles.ImageStyle}>
                  </Image>
                  <TextInput
                    placeholder="إدخل عدد الوحدات الفرعية بالصنف"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 188 / PixelRatio.get(), height: 32 / PixelRatio.get(), }]} />
                </View>
              </View>
              <View style={{ flex: .5, alignItems: 'center', justifyContent: 'flex-end' }}>
                <TouchableOpacity style={[styles.button, { width: 187 / PixelRatio.get(), height: 29 / PixelRatio.get(), }]}>
                  <Text numberOfLines={1} style={styles.buttonText}>إرسال</Text>
                </TouchableOpacity>


              </View>
            </KeyboardAwareScrollView>
          </ImageBackground>
        );
      } else if (width >= 320 && width < 480) {
        return (
          <ImageBackground style={styles.image}
            source={require('./images/320X480/splashbackground.png')} >
            <KeyboardAwareScrollView>
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Image style={{ width: 165 / PixelRatio.get(), height: 65 / PixelRatio.get(), resizeMode: 'center' }} source={require('./images/320X480/logo.png')} >
                </Image>
              </View>
              <View style={styles.container}>
                <View style={styles.SectionStyle}>
                  <Image source={require('./images/320X480/personicon.png')} style={styles.ImageStyle}>
                  </Image>
                  <TextInput
                    placeholder="اسم الصنف"
                    placeholderTextColor="black"
                    onChangeText={userName => this.setState({ userName })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 250 / PixelRatio.get(), height: 47 / PixelRatio.get(), }]} />

                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>
                  <Image source={require('./images/320X480/keyicon.png')} style={styles.ImageStyle}>
                  </Image>
                  <TextInput
                    placeholder="تاريخ الصلاحية"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 250 / PixelRatio.get(), height: 47 / PixelRatio.get(), }]} />
                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>
                  <Image source={require('./images/320X480/keyicon.png')} style={styles.ImageStyle}>
                  </Image>
                  <TextInput
                    placeholder="إدخل عدد الصنف"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 250 / PixelRatio.get(), height: 47 / PixelRatio.get(), }]} />
                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>
                  <Image source={require('./images/320X480/keyicon.png')} style={styles.ImageStyle}>
                  </Image>
                  <TextInput
                    placeholder="إدخل عدد الوحدات بالصنف"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 250 / PixelRatio.get(), height: 47 / PixelRatio.get(), }]} />
                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>
                  <Image source={require('./images/320X480/keyicon.png')} style={styles.ImageStyle}>
                  </Image>
                  <TextInput
                    placeholder="إدخل عدد الوحدات الفرعية بالصنف"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 250 / PixelRatio.get(), height: 47 / PixelRatio.get(), }]} />
                </View>
              </View>
              <View style={{ flex: .5, alignItems: 'center', justifyContent: 'flex-end' }}>
                <TouchableOpacity style={[styles.button, { width: 249 / PixelRatio.get(), height: 44 / PixelRatio.get(), }]}>
                  <Text numberOfLines={1} style={styles.buttonText}>إرسال</Text>
                </TouchableOpacity>


              </View>
            </KeyboardAwareScrollView>
          </ImageBackground>
        );
      }
      else if (width >= 480 && width < 540) {

        return (
          <ImageBackground style={styles.image}
            source={require('./images/480X800/splashbackground.png')} >
            <KeyboardAwareScrollView>
              <View style={{ alignItems: 'center', alignContent: 'center', flexDirection: 'column', margin: 10 }}>
                <Image style={{ width: 247 / PixelRatio.get(), height: 108 / PixelRatio.get(), resizeMode: 'center' }} source={require('./images/480X800/logo.png')} >
                </Image>
              </View>
              <View style={styles.container}>
                <View style={styles.SectionStyle}>
                  <TextInput
                    placeholder="اسم الصنف"
                    placeholderTextColor="black"
                    onChangeText={userName => this.setState({ userName })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 400 / PixelRatio.get(), height: 77 / PixelRatio.get(), }]} />

                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>

                  <TextInput
                    placeholder="تاريخ الصلاحية"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 400 / PixelRatio.get(), height: 77 / PixelRatio.get(), }]} />
                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>

                  <TextInput
                    placeholder="إدخل عدد الصنف"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 400 / PixelRatio.get(), height: 77 / PixelRatio.get(), }]} />
                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>

                  <TextInput
                    placeholder="إدخل عدد الوحدات بالصنف"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 400 / PixelRatio.get(), height: 77 / PixelRatio.get(), }]} />
                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>

                  <TextInput
                    placeholder="إدخل عدد الوحدات بالصنف"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 400 / PixelRatio.get(), height: 77 / PixelRatio.get(), }]} />
                </View>
              </View>
              <View style={{ flex: .5, alignItems: 'center', justifyContent: 'flex-end' }}>
                <TouchableOpacity style={[styles.button, { width: 372 / PixelRatio.get(), height: 73 / PixelRatio.get(), }]}>
                  <Text numberOfLines={1} style={styles.buttonText}>إرسال</Text>
                </TouchableOpacity>
              </View>
            </KeyboardAwareScrollView>
          </ImageBackground>
        );
      }
      else if (width >= 540 && width < 720) {
        return (
          <KeyboardAwareScrollView>
            <ImageBackground style={styles.image}
              source={require('./images/540X960/splashbackground.png')} >

              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Image style={{ width: 278 / PixelRatio.get(), height: 130 / PixelRatio.get(), resizeMode: 'center' }} source={require('./images/540X960/logo.png')} >
                </Image>
              </View>
              <View style={styles.container}>
                <View style={styles.SectionStyle}>

                  <TextInput
                    placeholder="اسم الصنف"
                    placeholderTextColor="black"
                    onChangeText={userName => this.setState({ userName })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 450 / PixelRatio.get(), height: 92 / PixelRatio.get(), }]} />
                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>

                  <TextInput
                    placeholder="تاريخ الصلاحية"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 450 / PixelRatio.get(), height: 92 / PixelRatio.get(), }]} />
                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>

                  <TextInput
                    placeholder="إدخل عدد الصنف"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 450 / PixelRatio.get(), height: 92 / PixelRatio.get(), }]} />
                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>

                  <TextInput
                    placeholder="إدخل عدد الوحدات بالصنف"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 450 / PixelRatio.get(), height: 92 / PixelRatio.get(), }]} />
                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>

                  <TextInput
                    placeholder="إدخل عدد الوحدات الفرعية بالصنف"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 450 / PixelRatio.get(), height: 92 / PixelRatio.get(), }]} />
                </View>
              </View>
              <View style={{ flex: .5, alignItems: 'center', justifyContent: 'flex-end' }}>
                <TouchableOpacity style={[styles.button, { width: 419 / PixelRatio.get(), height: 87 / PixelRatio.get(), }]}>
                  <Text numberOfLines={1} style={styles.buttonText}>إرسال</Text>
                </TouchableOpacity>

              </View>

            </ImageBackground>
          </KeyboardAwareScrollView>
        );
      }
      else if (width >= 720 && width < 800) {

        return (
          <ImageBackground style={styles.image}
            source={require('./images/720X1280/splashbackground.png')} >
            <KeyboardAwareScrollView>
              <View style={{ alignItems: 'center', alignContent: 'center', flexDirection: 'column' }}>
                <Image style={{ width: 372 / PixelRatio.get(), height: 173 / PixelRatio.get(), resizeMode: 'center', margin: 10 }}
                  source={require('./images/720X1280/logo.png')} >
                </Image>
              </View>
              <View style={styles.container}>
                <View style={styles.SectionStyle}>

                  <TextInput
                    editable={false}
                    placeholder={"إسم الصنف : " + name}
                    placeholderTextColor="gray"
                    onChangeText={userName => this.setState({ userName })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 600 / PixelRatio.get(), height: 123 / PixelRatio.get(), }]} />

                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>

                  <TextInput
                    editable={false}
                    placeholder={expiry}
                    placeholderTextColor="gray"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 600 / PixelRatio.get(), height: 123 / PixelRatio.get(), }]} />
                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>
                  <TextInput
                    placeholder="إدخل عدد الصنف"
                    value={this.state.packageText}
                    keyboardType={'number-pad'}
                    placeholderTextColor="black"
                    onChangeText={(text) => {
                      if (text == 1) {
                        this.setState({ 
                          packageText: text ,
                          unitsFlag: false
                        })
                      } else if (text == "") {
                        this.setState({ packageText: text,
                          unitsFlag: true })
                      }
                    }}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 600 / PixelRatio.get(), height: 123 / PixelRatio.get(), }]} />
                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>

                  <TextInput
                    editable={this.state.unitsFlag}
                    placeholder="إدخل عدد الوحدات بالصنف"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 600 / PixelRatio.get(), height: 123 / PixelRatio.get(), }]} />
                </View>
                <View style={[styles.SectionStyle, { margin: 10 }]}>

                  <TextInput
                    editable={this.state.unitsFlag}
                    placeholder="إدخل عدد الوحدات الفرعية بالصنف"
                    placeholderTextColor="black"
                    onChangeText={password => this.setState({ password })}
                    underlineColorAndroid='transparent'
                    style={[styles.TextInputUserName, { width: 600 / PixelRatio.get(), height: 123 / PixelRatio.get(), }]} />
                </View>
              </View>
              <View style={{ flex: .5, alignItems: 'center', justifyContent: 'flex-end' }}>
                <TouchableOpacity onPress={this.anotherFunction.bind(this)} style={[styles.button, { width: 559 / PixelRatio.get(), height: 116 / PixelRatio.get(), }]}>
                  <Text numberOfLines={1} style={styles.buttonText}>إرسال</Text>
                </TouchableOpacity>

              </View>
            </KeyboardAwareScrollView>
          </ImageBackground>
        );
      }
      else if (width >= 800 && width < 1080) {
        alert(width)
        return (
          <ImageBackground style={styles.image}
            source={require('./images/800X1280/splashbackground.png')} >
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Image style={{ width: 412 / PixelRatio.get(), height: 173 / PixelRatio.get(), resizeMode: 'center', margin: 10 }} source={require('./images/800X1280/logo.png')} >
              </Image>
            </View>
            <View style={styles.container}>
              <View style={styles.SectionStyle}>
                <TextInput
                  placeholder="اسم الصنف"
                  placeholderTextColor="black"
                  onChangeText={userName => this.setState({ userName })}
                  underlineColorAndroid='transparent'
                  style={[styles.TextInputUserName, { width: 650 / PixelRatio.get(), height: 223 * PixelRatio.get() }]} />
              </View>
              <View style={[styles.SectionStyle, { margin: 10 }]}>
                <TextInput
                  placeholder="تاريخ الصلاحية"
                  placeholderTextColor="black"
                  onChangeText={password => this.setState({ password })}
                  underlineColorAndroid='transparent'
                  style={[styles.TextInputUserName, { width: 650 / PixelRatio.get(), height: 123 / PixelRatio.get() }]} />
              </View>
              <View style={[styles.SectionStyle, { margin: 10 }]}>

                <TextInput
                  placeholder="إدخل عدد الصنف"
                  placeholderTextColor="black"
                  onChangeText={password => this.setState({ password })}
                  underlineColorAndroid='transparent'
                  style={[styles.TextInputUserName, { width: 650 / PixelRatio.get(), height: 123 / PixelRatio.get() }]} />
              </View>
              <View style={[styles.SectionStyle, { margin: 10 }]}>

                <TextInput
                  placeholder="إدخل عدد الوحدات بالصنف"
                  placeholderTextColor="black"
                  onChangeText={password => this.setState({ password })}
                  underlineColorAndroid='transparent'
                  style={[styles.TextInputUserName, { width: 650 / PixelRatio.get(), height: 123 / PixelRatio.get() }]} />
              </View>
              <View style={[styles.SectionStyle, { margin: 10 }]}>
                <TextInput
                  placeholder="إدخل عدد الوحدات الفرعية بالصنف"
                  placeholderTextColor="black"
                  onChangeText={password => this.setState({ password })}
                  underlineColorAndroid='transparent'
                  style={[styles.TextInputUserName, { width: 650 / PixelRatio.get(), height: 123 / PixelRatio.get() }]} />
              </View>
            </View>
            <View style={{ flex: .5, alignItems: 'center', justifyContent: 'flex-end' }}>
              <TouchableOpacity onPress={this.anotherFunction.bind(this)} style={[styles.button, { width: 620 / PixelRatio.get(), height: 116 / PixelRatio.get(), }]}>
                <Text numberOfLines={1} style={styles.buttonText}>إرسال</Text>
              </TouchableOpacity>
            </View>
          </ImageBackground>
        );
      }
      else if (width >= 1080 && width < 1440) {
        return (
          <ImageBackground style={styles.image}
            source={require('./images/1080X1920/splashbackground.png')} >
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Image style={{ width: 555 / PixelRatio.get(), height: 259 / PixelRatio.get(), resizeMode: 'center' }} source={require('./images/1080X1920/logo.png')} >
              </Image>
            </View>
            <View style={styles.container}>
              <View style={styles.SectionStyle}>
                <TextInput
                  placeholder="اسم الصنف"
                  placeholderTextColor="black"
                  onChangeText={userName => this.setState({ userName })}
                  underlineColorAndroid='transparent'
                  style={[styles.TextInputUserName, { width: 860 / PixelRatio.get(), height: 185 / PixelRatio.get() }]} />
              </View>
              <View style={[styles.SectionStyle, { margin: 10 }]}>

                <TextInput
                  placeholder="تاريخ الصلاحية"
                  placeholderTextColor="black"
                  onChangeText={password => this.setState({ password })}
                  underlineColorAndroid='transparent'
                  style={[styles.TextInputUserName, { width: 860 / PixelRatio.get(), height: 185 / PixelRatio.get() }]} />
              </View>
              <View style={[styles.SectionStyle, { margin: 10 }]}>
                <TextInput
                  placeholder="إدخل عدد الصنف"
                  placeholderTextColor="black"
                  onChangeText={password => this.setState({ password })}
                  underlineColorAndroid='transparent'
                  style={[styles.TextInputUserName, { width: 860 / PixelRatio.get(), height: 185 / PixelRatio.get() }]} />
              </View>
              <View style={[styles.SectionStyle, { margin: 10 }]}>
                <TextInput
                  placeholder="إدخل عدد الوحدات بالصنف"
                  placeholderTextColor="black"
                  onChangeText={password => this.setState({ password })}
                  underlineColorAndroid='transparent'
                  style={[styles.TextInputUserName, { width: 860 / PixelRatio.get(), height: 185 / PixelRatio.get() }]} />
              </View>
              <View style={[styles.SectionStyle, { margin: 10 }]}>
                <TextInput
                  placeholder="إدخل عدد الوحدات الفرعية بالصنف"
                  placeholderTextColor="black"
                  onChangeText={password => this.setState({ password })}
                  underlineColorAndroid='transparent'
                  style={[styles.TextInputUserName, { width: 860 / PixelRatio.get(), height: 185 / PixelRatio.get() }]} />
              </View>
            </View>
            <View style={{ flex: .5, alignItems: 'center', justifyContent: 'flex-end' }}>
              <TouchableOpacity onPress={this.anotherFunction.bind(this)} style={[styles.button, { width: 837 / PixelRatio.get(), height: 173 / PixelRatio.get(), }]}>
                <Text numberOfLines={1} style={styles.buttonText}>إرسال</Text>
              </TouchableOpacity>
            </View>
          </ImageBackground>
        );
      }
      else if (width >= 1440) {
        return (
          <ImageBackground style={styles.image}
            source={require('./images/1440X2560/splashbackground.png')} >
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Image style={{ width: 739 / PixelRatio.get(), height: 344 / PixelRatio.get(), resizeMode: 'center' }} source={require('./images/1440X2560/logo.png')} >
              </Image>
            </View>
            <View style={styles.container}>
              <View style={styles.SectionStyle}>
                <TextInput
                  placeholder="اسم الصنف"
                  placeholderTextColor="black"
                  onChangeText={userName => this.setState({ userName })}
                  underlineColorAndroid='transparent'
                  style={[styles.TextInputUserName, { width: 1150 / PixelRatio.get(), height: 245 * PixelRatio.get() }]} />
              </View>
              <View style={[styles.SectionStyle, { margin: 10 }]}>
                <TextInput
                  placeholder="تاريخ الصلاحية"
                  placeholderTextColor="black"
                  onChangeText={password => this.setState({ password })}
                  underlineColorAndroid='transparent'
                  style={[styles.TextInputUserName, { width: 1150 / PixelRatio.get(), height: 245 / PixelRatio.get() }]} />
              </View>
              <View style={[styles.SectionStyle, { margin: 10 }]}>
                <TextInput
                  placeholder="إدخل عدد الصنف"
                  placeholderTextColor="black"
                  onChangeText={password => this.setState({ password })}
                  underlineColorAndroid='transparent'
                  style={[styles.TextInputUserName, { width: 1150 / PixelRatio.get(), height: 245 / PixelRatio.get() }]} />
              </View>
              <View style={[styles.SectionStyle, { margin: 10 }]}>
                <TextInput
                  placeholder="إدخل عدد الوحدات بالصنف"
                  placeholderTextColor="black"
                  onChangeText={password => this.setState({ password })}
                  underlineColorAndroid='transparent'
                  style={[styles.TextInputUserName, { width: 1150 / PixelRatio.get(), height: 245 / PixelRatio.get() }]} />
              </View>
              <View style={[styles.SectionStyle, { margin: 10 }]}>
                <TextInput
                  placeholder="إدخل عدد الوحدات الفرعية بالصنف"
                  placeholderTextColor="black"
                  onChangeText={password => this.setState({ password })}
                  underlineColorAndroid='transparent'
                  style={[styles.TextInputUserName, { width: 1150 / PixelRatio.get(), height: 245 / PixelRatio.get() }]} />
              </View>
            </View>
            <View style={{ flex: .5, alignItems: 'center', justifyContent: 'flex-end' }}>
              <TouchableOpacity onPress={this.anotherFunction.bind(this)} style={[styles.button, { width: 1116 / PixelRatio.get(), height: 225 / PixelRatio.get(), }]}>
                <Text numberOfLines={1} style={styles.buttonText}>إرسال</Text>
              </TouchableOpacity>
            </View>
          </ImageBackground>
        );
      }
    } else {
      if (width >= 640 && width < 750) {
        return (


          <ImageBackground style={styles.image}
            source={require('./images/iPhone5/Layer-27.png')} >
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Image style={{ width: 329, height: 153 }} source={require('./images/iPhone5/Layer-19.png')} >
              </Image>
            </View>
          </ImageBackground>
        );
      } else if (width >= 750 && width < 1080) {
        return (
          <ImageBackground style={styles.image}
            source={require('./images/iphone6/Layer-28.png')} >
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Image style={{ width: 329, height: 153 }} source={require('./images/iphone6/background.png')} >
              </Image>
            </View>
          </ImageBackground>
        );
      }
      else if (width >= 1080 && width < 1440) {
        return (
          <ImageBackground style={styles.image}
            source={require('./images/1080X1920/splashbackground.png')} >
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Image style={{ width: 555, height: 259, resizeMode: 'center' }} source={require('./images/1080X1920/logo.png')} >
              </Image>
            </View>
          </ImageBackground>
        );
      }
    }
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10
  },

  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    // resizeMode: 'contain'
  },
  ImageStyle: {
    // padding: 5,
    // margin: 5,
    height: 32 * PixelRatio.get(),
    width: 27 * PixelRatio.get(),
    backgroundColor: "#373333",
    resizeMode: 'center',
    alignItems: 'center',
  },
  SectionStyle: {
    flexDirection: 'row-reverse',
    // justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'transparent',
    // borderWidth: .5,
    // borderColor: '#000',
    height: 60,
    //borderRadius: 5 ,
    // margin:5
  },
  logo: {
    width: 412,
    height: 173,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  TextInputUserName: {
    textAlign: 'center',
    fontStyle: 'italic',
    fontFamily: 'didot',
    fontWeight: 'bold',
    opacity: 0.6,
    margin: 5,
    color: 'white',
    fontSize: 16,
    backgroundColor: "#373333",
  },
  TextForPassword: {
    textAlign: 'center',
    color: '#0F0B5E',
    textDecorationLine: 'underline',
    fontWeight: 'bold',
    height: 60,
    width: width / 3,
    margin: 5,
    fontSize: 20,
  },
  checkBoxText: {
    textAlign: 'center',
    color: '#0F0B5E',
    textDecorationLine: 'underline',
    // fontStyle: 'italic',
    // fontFamily: 'didot',
    fontWeight: 'bold',
    //margin: 10,
    fontSize: 20,
  },
  button: {
    borderRadius: 27,
    backgroundColor: '#1e2f4d',
    justifyContent: 'center',
    opacity: .9,
  },
  buttonText: {
    fontWeight: 'bold',
    fontSize: 14,
    color: 'white',
    textAlign: 'center',
  },
});

