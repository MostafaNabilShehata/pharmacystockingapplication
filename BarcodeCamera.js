/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component, PureComponent } from "react";
import { Dimensions, Text, StyleSheet, View, Platform, Animated, Easing } from "react-native";
import { NavigationEvents } from 'react-navigation';
import { RNCamera } from 'react-native-camera';

export default class BarcodeCamera extends Component {

  constructor() {
    super();
    this.state = {
      is_camera: 1, // START CAMERA
      notification: "",
      value: "",
      opacity: new Animated.Value(1),
      offset: new Animated.Value(0),
    }
    this.animatedValue = new Animated.Value(0)
  }

  componentDidMount() {
    // ANIMATION CALL
    this.animation_method()
    this.timer = setInterval(() => this.animation_method(), 6000)

  }
  // BEGIN BARCODE READ DATA METHOD
  onBarCodeReads = (data) => {
    if (data.data.length == 12) {
      fetch('http://41.129.129.86:8080//UbiPharmacy/searchdrugs?action=searchdrug&searchkey=localbarcode&searchvalue='+data.data.slice(1, 6))
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status == "success") {
            this.props.navigation.navigate('Counter', {
              item: (responseJson.drugs[0])
            })
          }else{
            alert("لا يوجد بيانات")
          }
        })
        .catch((error) => {
          alert(error);
        });
    } else {
      fetch('http://41.129.129.86:8080//UbiPharmacy/searchdrugs?action=searchdrug&searchkey=intbarcode&searchvalue='+data.data)
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status == "success") {
            alert(JSON.stringify(responseJson.drugs).name)
            // this.props.navigation.navigate('Counter', {
            //   item: JSON.stringify(responseJson.drugs)
            // })
          }else{
            alert("لا يوجد بيانات")
          }
        })
        .catch((error) => {
          alert(error);
        });
    }
  }
  // END BARCODE READ DATA METHOD

  // BEGIN ANIMATION METHOD
  animation_method() {

    this.state.offset.setValue(2 * -1);

    Animated.sequence([

      Animated.parallel([
        Animated.timing(this.state.opacity, {
          toValue: 1,
          duration: 3000
        }),
        Animated.timing(this.state.offset, {
          toValue: Platform.OS == 'android' ? (Dimensions.get('window').height / 3) - 80 : (Dimensions.get('window').height / 3) - 50,
          duration: 3000
        }),
      ]),

      Animated.delay(0),

      Animated.parallel([
        Animated.timing(this.state.opacity, {
          toValue: 1,
          duration: 3000
        }),
        Animated.timing(this.state.offset, {
          toValue: 2 * -1,
          duration: 3000
        }),
      ]),

    ]).start();


  }
  // END ANIMATION METHOD

  render() {
    // BEGIN ANIMATION STYLE
    const animationStyle = {
      opacity: this.state.opacity,
      transform: [
        {
          translateY: this.state.offset,
        },
      ],
    };
    // END ANIMATION STYLE

    const { height, width } = Dimensions.get('window');
    const maskRowHeight = 30;
    const maskColWidth = (width - 300) / 2;

    return (
      <View style={[BarcodeStyles.container, { position: 'absolute', top: 0, right: 0, bottom: 0, left: 0 }]}>

        {/* BEGIN ONLOAD VIEW METHOD FOR RESTART CAMERA */}
        <NavigationEvents
          onWillFocus={payload =>
            setTimeout(() => {

              this.setState({
                is_camera: 1
              })

            }, 50)
          } // ON VIEW FOCUS EVENT

          onWillBlur={payload =>
            setTimeout(() => {
              this.setState({
                is_camera: 0
              })
            }, 50)
          } // ON VIEW BLUR EVENT
        />
        {/* END ONLOAD VIEW METHOD FOR RESTART CAMERA */}


        {/* BEGIN CAMERA VIEW FOR IOS */}
        {(this.state.is_camera == 1 && Platform.OS == 'ios') ?
          <RNCamera
            ref={cam => {
              this.camera = cam;
            }}
            onBarCodeRead={this.onBarCodeReads}
            style={BarcodeStyles.cameraView}
            playSoundOnCapture
          >

            <View style={BarcodeStyles.maskOutter}>
              <View style={[{ flex: maskRowHeight }, BarcodeStyles.maskRow, BarcodeStyles.maskFrame]} />
              <View style={[{ flex: 30 }, BarcodeStyles.maskCenter]}>
                <View style={[{ width: maskColWidth }, BarcodeStyles.maskFrame]} />
                <View style={BarcodeStyles.maskInner} />
                <View style={[{ width: maskColWidth }, BarcodeStyles.maskFrame]} />
              </View>

              <View style={[{ flex: maskRowHeight }, BarcodeStyles.maskRow, BarcodeStyles.maskFrame]} />
            </View>
          </RNCamera> : null}
        {/* END CAMERA VIEW FOR IOS */}

        {/* BEGIN CAMERA VIEW FOR ANDROID */}
        {(this.state.is_camera == 1 && Platform.OS == 'android') ?
          <RNCamera
            ref={cam => {
              this.camera = cam;
            }}
            onGoogleVisionBarcodesDetected={({ barcodes }) => { this.onBarCodeReads(barcodes[0]) }}
            style={BarcodeStyles.cameraView}
            playSoundOnCapture
          >
            <View style={BarcodeStyles.maskOutter}>
              <View style={[{ flex: maskRowHeight }, BarcodeStyles.maskRow, BarcodeStyles.maskFrame]} />
              <View style={[{ flex: 30 }, BarcodeStyles.maskCenter]}>
                <View style={[{ width: maskColWidth }, BarcodeStyles.maskFrame]} />
                <View style={BarcodeStyles.maskInner} />
                <View style={[{ width: maskColWidth }, BarcodeStyles.maskFrame]} />
              </View>

              <View style={[{ flex: maskRowHeight }, BarcodeStyles.maskRow, BarcodeStyles.maskFrame]} />
            </View>
          </RNCamera> : null}
        {/* END CAMERA VIEW FOR ANDROID */}

        {(this.state.is_camera == 1 && Platform.OS == 'ios') ?

          <Animated.View
            style={[BarcodeStyles.animation, animationStyle, { backgroundColor: '#5BBE3F', top: (Dimensions.get('window').height / 3) }]}

          >
          </Animated.View>

          : null}

        {(this.state.is_camera == 1 && Platform.OS == 'android') ?

          <Animated.View
            style={[BarcodeStyles.animation, animationStyle, { backgroundColor: '#5BBE3F', top: Dimensions.get('window').height / 3 - 30 }]}

          >
          </Animated.View>

          : null}

      </View>
    );
  }
}

const BarcodeStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
  animation: {
    position: "absolute",
    paddingHorizontal: 0,
    paddingVertical: 0,
    alignSelf: 'center',
    height: 2,
    width: Dimensions.get('window').width - 80
  },
  cameraView: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  maskOutter: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  maskInner: {
    width: Dimensions.get('window').width - 5,
    backgroundColor: 'transparent',
    borderColor: 'white',
    borderWidth: 1,
  },
  maskFrame: {
    backgroundColor: 'rgba(1,1,1,0.6)',
  },
  maskRow: {
    width: '100%',
  },
  maskCenter: { flexDirection: 'row' },
});
